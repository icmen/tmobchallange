package com.tmob.michallange.api;

public enum ApiRequestStatus {
    LOADING,
    FINISH,
    SUCCESS,
    ERROR,
    FAIL
}
